# Neurosurgery.duke.edu/ Assessment

__<https://neurosurgery.duke.edu/>__

__Screenshot:__

![Screenshot of this website](assets/screenshot.png)

<div id="toc">
<!--TOC-->
</div>

<br>
<hr>

# Accessibility

These checks highlight opportunities to [improve the accessibility of your web app](https://developers.google.com/web/fundamentals/accessibility). Only a subset of accessibility issues can be automatically detected so manual testing is also encouraged.

These items address areas which an automated testing tool cannot cover. Learn more in our guide on [conducting an accessibility review](https://developers.google.com/web/fundamentals/accessibility/how-to-review).

















## Background and foreground colors do not have a sufficient contrast ratio. [WCAG 1.4.3](https://www.w3.org/WAI/WCAG21/quickref/?versions=2.0#contrast-minimum)

Low-contrast text is difficult or impossible for many users to read. [Learn more](https://web.dev/color-contrast/).



### The element _"Contact Us"_ has low contrast.

__Visual location:__

![Text with low contrast](assets/neurosurgery-duke-edu-li-nth-child-2-a-href-contact-us.png)


__HTML location:__

```html
<a href="/contact-us">Contact Us</a>
```

#### Suggested solution:

  Element has insufficient color contrast of 4.17 (foreground color: #ffffff, background color: #d94b3d, font size: 10.5pt, font weight: normal). Expected contrast ratio of 4.5:1

[Find a color with higher contrast](http://contrast-finder.tanaguru.com).

<details>
<summary>_Additional debugging details_</summary>
Path:<br>
`1,HTML,1,BODY,1,HEADER,0,DIV,0,DIV,0,DIV,0,SECTION,1,UL,1,LI,0,A`<br>
Selector:<br>
`li:nth-child(2) > a[hrefS="contact-us"]`
</details>

---



### The element _"HIGHLIGHT"_ has low contrast.

__Visual location:__

![Text with low contrast](assets/neurosurgery-duke-edu-box-green-p-nth-child-1.png)


__HTML location:__

```html
<p>HIGHLIGHT</p>
```

#### Suggested solution:

  Element has insufficient color contrast of 3.4 (foreground color: #ffffff, background color: #809421, font size: 10.5pt, font weight: normal). Expected contrast ratio of 4.5:1

[Find a color with higher contrast](http://contrast-finder.tanaguru.com).

<details>
<summary>_Additional debugging details_</summary>
Path:<br>
`1,HTML,1,BODY,4,DIV,0,DIV,0,DIV,0,DIV,0,SECTION,0,DIV,0,P`<br>
Selector:<br>
`.box-green > p:nth-child(1)`
</details>

---



### The element _"Duke Spine Center"_ has low contrast.

__Visual location:__

![Text with low contrast](assets/neurosurgery-duke-edu-box-green-big-nth-child-3.png)


__HTML location:__

```html
<p class="big">Duke Spine Center</p>
```

#### Suggested solution:

  Element has insufficient color contrast of 3.4 (foreground color: #ffffff, background color: #809421, font size: 15.8pt, font weight: normal). Expected contrast ratio of 4.5:1

[Find a color with higher contrast](http://contrast-finder.tanaguru.com).

<details>
<summary>_Additional debugging details_</summary>
Path:<br>
`1,HTML,1,BODY,4,DIV,0,DIV,0,DIV,0,DIV,0,SECTION,0,DIV,2,P`<br>
Selector:<br>
`.box-green > .big:nth-child(3)`
</details>

---



### The element _"Learn More"_ has low contrast.

__Visual location:__

![Text with low contrast](assets/neurosurgery-duke-edu-box-green-p-nth-child-4-btn-target-_blank.png)


__HTML location:__

```html
<a class="btn" href="https://www.dukehealth.org/locations/duke-spine-center" target="_blank">Learn More</a>
```

#### Suggested solution:

  Element has insufficient color contrast of 3.4 (foreground color: #ffffff, background color: #809421, font size: 10.5pt, font weight: normal). Expected contrast ratio of 4.5:1

[Find a color with higher contrast](http://contrast-finder.tanaguru.com).

<details>
<summary>_Additional debugging details_</summary>
Path:<br>
`1,HTML,1,BODY,4,DIV,0,DIV,0,DIV,0,DIV,0,SECTION,0,DIV,3,P,0,A`<br>
Selector:<br>
`.box-green > p:nth-child(4) > .btn[target="_blank"]`
</details>

---



### The element _"3Rank Based on Funding by NIH"_ has low contrast.

__Visual location:__

![Text with low contrast](assets/neurosurgery-duke-edu-green-.png)


__HTML location:__

```html
<p><span class="number">3</span>Rank Based on<br>
Funding by NIH</p>
```

#### Suggested solution:

  Element has insufficient color contrast of 2.85 (foreground color: #809421, background color: #ebebeb, font size: 12.6pt, font weight: normal). Expected contrast ratio of 4.5:1

[Find a color with higher contrast](http://contrast-finder.tanaguru.com).

<details>
<summary>_Additional debugging details_</summary>
Path:<br>
`1,HTML,1,BODY,4,DIV,0,DIV,0,DIV,0,DIV,1,SECTION,1,DIV,1,DIV,0,P`<br>
Selector:<br>
`.green > p`
</details>

---



### The element _"24Percent of Residents Who Are Women"_ has low contrast.

__Visual location:__

![Text with low contrast](assets/neurosurgery-duke-edu-red-.png)


__HTML location:__

```html
<p><span class="number">24</span>Percent of Residents<br>
Who Are Women</p>
```

#### Suggested solution:

  Element has insufficient color contrast of 3.5 (foreground color: #d94b3d, background color: #ebebeb, font size: 12.6pt, font weight: normal). Expected contrast ratio of 4.5:1

[Find a color with higher contrast](http://contrast-finder.tanaguru.com).

<details>
<summary>_Additional debugging details_</summary>
Path:<br>
`1,HTML,1,BODY,4,DIV,0,DIV,0,DIV,0,DIV,1,SECTION,1,DIV,2,DIV,0,P`<br>
Selector:<br>
`.red > p`
</details>

---




## Links do not have a discernible name [WCAG 4.1.2](https://www.w3.org/WAI/WCAG21/quickref/?versions=2.0#name-role-value)

Link text (and alternate text for images, when used as links) that is discernible, unique, and focusable improves the navigation experience for screen reader users. [Learn more](https://web.dev/link-name/).



### Search link has no value [Critical]

Link has no screen reader text to indicate its purpose.  It would just read the work "Link" to a screen reader user.  Text does not need to be visible to sighted users, but must exist in the code.

As a result, screen reader users would not be able to use the websites search.

__Visual location:__

![  not descriptive](assets/neurosurgery-duke-edu-top-links-li-nth-child-1-a-href-search.png)

#### HTML location:

```html
<a href="/search"><i aria-hidden="true" class="fa fa-search">&nbsp;</i></a>
```

#### Suggested solution:

Search link has no text to indicate its purpose.  Add aria-label="Search" to link or add invisible screen reader only text.

<details>
<summary>_Other options:_</summary>
Fix all of the following:
<br>Element is in tab order and does not have accessible text

Fix any of the following:
<br>Element does not have text that is visible to screen readers
<br>aria-label attribute does not exist or is empty
<br>aria-labelledby attribute does not exist, references elements that do not exist or references elements that are empty
<br>Element&#39;s default semantics were not overridden with role=&#34;presentation&#34;
<br>Element&#39;s default semantics were not overridden with role=&#34;none&#34;
</details>

<details>
<summary>_Additional debugging details_</summary>
Path:<br>
`1,HTML,1,BODY,1,HEADER,0,DIV,0,DIV,0,DIV,0,SECTION,1,UL,0,LI,0,A`<br>
Selector:<br>
`.top-links > li:nth-child(1) > a[hrefS="search"]`
</details>

<br>

---



### Social media links have no value

Link has no screen reader text to indicate its purpose.  It would just read the work "Link" to a screen reader user.  Text does not need to be visible to sighted users, but must exist in the code.

__Visual location:__

![  not descriptive](assets/neurosurgery-duke-edu-li-nth-child-1-a-target-_blank.png)

![  not descriptive](assets/neurosurgery-duke-edu-li-nth-child-2-a-target-_blank.png)

#### HTML location:

```html
<a href="https://twitter.com/dukeneurosurg" target="_blank"><i aria-hidden="true" class="fa fa-twitter fa-2x">&nbsp;</i></a>
```

```html
<a href="https://www.facebook.com/dukeneurosurgery" target="_blank"><i aria-hidden="true" class="fa fa-facebook-official fa-2x">&nbsp;</i></a>
```

#### Suggested solution:

Add aria-label="Twitter" to link or add invisible screen reader only text.

 Add aria-label="Facebook" to link or add invisible screen reader only text.

<details>
<summary>_Other options:_</summary>
Fix all of the following:
<br>Element is in tab order and does not have accessible text

Fix any of the following:
<br>Element does not have text that is visible to screen readers
<br>aria-label attribute does not exist or is empty
<br>aria-labelledby attribute does not exist, references elements that do not exist or references elements that are empty
<br>Element&#39;s default semantics were not overridden with role=&#34;presentation&#34;
<br>Element&#39;s default semantics were not overridden with role=&#34;none&#34;
</details>

<details>
<summary>_Additional debugging details_</summary>
Path:<br>
`1,HTML,1,BODY,1,HEADER,0,DIV,0,DIV,0,DIV,0,SECTION,0,UL,0,LI,0,A`<br>
Selector:<br>
`li:nth-child(1) > a[target="_blank"]`
</details>

<br>

---



### FYI: What is 'invisible screen reader only' text?

Screen reader only text is text that is read out-loud to a screen reader user, but not displayed on the screen. Most websites already have a class for this called `.element-invisible` or `.sr-only`. If this website does not, add this class to a stylesheet:

```css
.sr-only {
  position: absolute;
  width: 1px;
  height: 1px;
  padding: 0;
  margin: -1px;
  overflow: hidden;
  clip: rect(0,0,0,0);
  border: 0;
}
```

Example usage:

```html
<a class="read-more" href="fancy-cats.html">
    Read more
+    <span class="sr-only"> about fancy cats</span>
  </i>
</a>
```

---




## The page has a logical tab order [WCAG 2.4.3](https://www.w3.org/WAI/WCAG21/quickref/?versions=2.0#focus-order)

__I need a human!__ Manual Test: The page has a logical tab order

Description:<br>
Tabbing through the page follows the visual layout. Users cannot focus elements that are offscreen. [Learn more](https://web.dev/logical-tab-order/).

__Success!__

---

<br>

## Interactive controls are keyboard focusable [WCAG 2.1.1](https://www.w3.org/WAI/WCAG21/quickref/?versions=2.0#keyboard)

__I need a human!__ Manual Test: Interactive controls are keyboard focusable [Critical]

Description:<br>
Custom interactive controls are keyboard focusable and display a focus indicator. [Learn more](https://web.dev/focusable-controls/).

The key purpose of :focus is to give a user guidance. 

Links need to be focusable as the user tabs through the website for keyboard only users and screen reader users. If the focus indicator is missing, a keyboard user will have no idea what link they are on. [Learn more](https://www.deque.com/blog/accessible-focus-indicators/)

By default the :focus styles work in a webbrowser.  . Links, Buttons, and Form Fields / Controls (text fields, select boxes, radio buttons, etc.) are focusable. It is possible to inadvertently break this behavior.

Custom :focus states are allowed. The :focus states in the main content of this website are valid.

### Main menu toggles missing outline

__Example:__

<video>
<source src="assets/focus-indicator.mp4"  type="video/mp4">
</video>

__Code location of menu with no focus indicator:__

The offending code can be found in bootstrap.css:

```css
.dropdown-toggle:focus {
    outline: 0;
}
```

### Suggested solution:

The best way to fix this is to delete any references to outline, outline-width, outline-color, properties so it restores the browsers valid and compliant behavior.

If that CSS/SASS can not be modified for some reason it be fixed in an additive manner by overriding the outline with a new outline with enough specificity to override the errant code.

Add this to provide a visible `:focus` state:

```css
.dropdown .dropdown-toggle:focus {
  background: #ccc;
}
```

Or any other valid `:focus` style.

---

<br>


## Interactive elements indicate their purpose and state  [WCAG 1.4.1](https://www.w3.org/WAI/WCAG21/quickref/#use-of-color) [WCAG 1.3.3](https://www.w3.org/WAI/WCAG21/quickref/#sensory-characteristics)


__I need a human!__ Manual Test: Interactive elements indicate their purpose and state

Description:<br>
Interactive elements, such as links and buttons, should indicate their state and be distinguishable from non-interactive elements. [Learn more](https://web.dev/interactive-element-affordance/).

Pay special attention to buttons and links.  For example, links and buttons should have obvious :hover and :focus states that meet WCAG 2.0 AA contrast requirements.

__Needs work.__

Most of the hover states in the main content area are great.  Congrats! But the side submenus are missing `:hover` states.

When a user hovers over a link, it needs to change in some way to indicate that the mouse is hovering over a clickable element. The mouse pointer alone is not compliant. It must change color or shape in some way.

__Visual location:__

![missing hover](assets/side-submenu.png)

__HTML location:__

```html
<div class="menu-block-wrapper menu-block-2 menu-name-main-menu parent-mlid-0 menu-level-2">
  <ul class="menu nav"><li class="first leaf menu-mlid-552"><a href="/about">Overview</a></li>
<li class="leaf menu-mlid-566"><a href="/about/chair-department-neurosurgery">Interim Chair</a></li>
...
<li class="leaf menu-mlid-572"><a href="/about/contact-us">Contact Us</a></li>
<li class="last leaf menu-mlid-1401"><a href="https://intranet.medschool.duke.edu/depts/nsu/SitePages/Home.aspx" title="">NSU Intranet</a></li>
</ul></div>
```

The active page is indicated in light blue which is great.

But, in this example the mouse is `:hover`ing over the "Careers" link, but it looks the same as the other links.


#### Suggested solution:

Add a `:hover` state.  Do whatever you want, as long as it is distinguishable from the normal state.

This would work as an example:

```css
#primary .menu > li > a.active:hover {
    background-color: rgba(0, 76, 149, 0.3);
}
#primary .menu > li > a:hover {
    background-color: rgba(0, 76, 149, 0.2);
}
```

---

<br>



## The user's focus is directed to new content added to the page [WCAG 3.2.2](https://www.w3.org/WAI/WCAG21/quickref/?versions=2.0#on-input)

__I need a human!__ Manual Test: The user&#39;s focus is directed to new content added to the page

Description:<br>
If new content, such as a dialog, is added to the page, the user&#39;s focus is directed to it. [Learn more](https://web.dev/managed-focus/).

__Needs work.__

### Screen reader users do not have access to dropdown navigation

If a screen reader user tries to activate drop down menu links, ,like "About", they are not alerted that new items are available.

As a result, they would not have access to most of the website.

#### Suggested solution:

Easy fix. Paste the code below into a JS file. If this is Drupal, put it in a behavior.  If not, put in a normal jQuery document.ready.

```js
// add missing aria attributes
jQuery('a.dropdown-toggle').attr('aria-haspopup', 'true').attr('aria-expanded', 'false');

// Handle link
jQuery('a.dropdown-toggle').bind('click', function (e) {
    // get current expanded state
    var expandedState = jQuery(this).attr('aria-expanded');
    // toggle all to false
    jQuery('a.dropdown-toggle').attr('aria-expanded', 'false');

    if (expandedState == 'true') { 
        jQuery(this).attr('aria-expanded', 'false');
    }
    if (expandedState == 'false') {
        jQuery(this).attr('aria-expanded', 'true');
    }
});
```


---

<br>



## User focus is not accidentally trapped in a region [WCAG 2.1.2](https://www.w3.org/WAI/WCAG21/quickref/?versions=2.0#no-keyboard-trap)

__I need a human!__ Manual Test: User focus is not accidentally trapped in a region

Description:<br>
A user can tab into and out of any control or region without accidentally trapping their focus. [Learn more](https://web.dev/focus-traps/).

__Success!__

---

<br>




## Custom controls have associated labels [WCAG 4.1.2](https://www.w3.org/WAI/WCAG21/quickref/?versions=2.0#name-role-value)

__I need a human!__ Manual Test: Custom controls have associated labels

Description:<br>
Custom interactive controls have associated labels, provided by aria-label or aria-labelledby. [Learn more](https://web.dev/custom-controls-labels/).

__Success!__

---

<br>



## Custom controls have ARIA roles [WCAG 4.1.2](https://www.w3.org/WAI/WCAG21/quickref/?versions=2.0#name-role-value)

__I need a human!__ Manual Test: Custom controls have ARIA roles

Description:<br>
Custom interactive controls have appropriate ARIA roles. [Learn more](https://web.dev/custom-control-roles/).

__Success after main menu expand collapse ARIA attributes are fixed from earlier__

---

<br>



## Visual order on the page follows DOM order [WCAG 1.3.2](https://www.w3.org/WAI/WCAG21/quickref/?versions=2.0#meaningful-sequence)

__I need a human!__ Manual Test: Visual order on the page follows DOM order

Description:<br>
DOM order matches the visual order, improving navigation for assistive technology. [Learn more](https://web.dev/visual-order-follows-dom/).


__Success!__

---






## Offscreen content is hidden from assistive technology [WCAG 2.1.1](https://www.w3.org/WAI/WCAG21/quickref/?versions=2.0#keyboard) [WCAG 2.4.3](https://www.w3.org/WAI/WCAG21/quickref/?versions=2.0#focus-order)

__I need a human!__ Manual Test: Offscreen content is hidden from assistive technology

Description:<br>
Offscreen content is hidden with display: none or aria-hidden=true. [Learn more](https://web.dev/offscreen-content-hidden/).

Pay special attention to menus. For example, the focus indicator should not be lost while tabbing through a menu.

__Success!__

---

<br>


## Headings don't skip levels [WCAG 2.4.6](https://www.w3.org/WAI/WCAG21/quickref/?versions=2.0#headings-and-labels)

__I need a human!__ Manual Test: Headings don&#39;t skip levels

Description:<br>
Headings are used to create an outline for the page and heading levels are not skipped. [Learn more](https://web.dev/heading-levels/).

__Success!__

---

<br>



## HTML5 landmark elements are used to improve navigation [WCAG 1.3.1](https://www.w3.org/WAI/WCAG21/quickref/?versions=2.0#info-and-relationships) [WCAG 2.4.1](https://www.w3.org/WAI/WCAG21/quickref/?versions=2.0#bypass-blocks)

__I need a human!__ Manual Test: HTML5 landmark elements are used to improve navigation

Description:<br>
Landmark elements (&lt;main&gt;, &lt;nav&gt;, etc.) are used to improve the keyboard navigation of the page for assistive technology. [Learn more](https://web.dev/use-landmarks/).

__Needs work.__

### Unlabeled duplicate Landmarks and missing Landmarks.

Missing a `<main>` landmark.

Nested `<header>` Landmark.

#### Suggested solution:

Put all the header content inside one `<header>`, without nesting.

Put all content between the `<header>` and the `<footer>` in a `<main>`.




---

<br>


<hr>
<hr>

This accessibility assessment was generated from a [Chrome Lighthouse](https://developers.google.com/web/tools/lighthouse/) report. The Accessibility portion of Chrome Lighthouse is largely sourced from [Deque's Axe-core](https://github.com/dequelabs/axe-core) engine.

Thorough testing should also include testing with the [WAVE Web Accessibility Evaluation tool](http://wave.webaim.org/).

Accessibility testing also requires a human to determine the validity and seriousness of an issue. Automated tests like this only catch about 40% of accessibility issues. For example, automated tests cannot tell if a menu is keyboard accessible. Please follow the checklist of manual items that require a human to test. 

You can re-run the automated section of this report yourself using the open-source [OpenAssessIt project](https://github.com/OpenAssessItToolkit/openassessit) on GitHub.